/*
 * starter-kit-esp8266
 * version: 1.0
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>


#define mqtt_server "maqiatto.com"

// exemple : Livebox-5F63
const char* ssid     = "votre-ssid";

// Clé de sécurité Wifi: A66F6F6ABCF646E416A2EE8150
const char* password = "leMotPasse";

#define mqtt_user "fablabgsvXXXXXX"  // Le compte maqiatto
#define mqtt_password "leMotPasse" // le mot de passe maqiatto

// topic en écoute 
#define alerte_sub_topic "fablabgsv@gmail.com/hello"  

#define DEBUG 0

//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

WiFiClient espClient;
PubSubClient client(espClient);

void setup()
{
  Serial.begin(9600);

  Wire.begin();
   
   
  setup_wifi(); //On se connecte au réseau wifi

  // Configuration de la connexion au serveur MQTT
  client.setServer(mqtt_server, 1883);
  
  // La fonction de callback qui est executée à chaque réception de message   
  client.setCallback(callback);  

  // Envoi d'un message pour indiquer que l'esp vient de démarrer.
  reconnect();
  Serial.print("message de demarrage.");
}

//Connexion au réseau WiFi
void setup_wifi() {
  delay(10);

  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connexion a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());

}

/**
 * Reconnexion
 */
void reconnect() {
  //Boucle jusqu'à obtenir une reconnexion
 
  while (!client.connected()) {    
    Serial.print("Connexion au serveur MQTT...");
    if (client.connect("esp8266", mqtt_user, mqtt_password)) {
      Serial.println("mqtt connect OK");
      client.subscribe(alerte_sub_topic);
      return;
    } else {
      Serial.print("mqtt connect KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 5 secondes avant de recommencer");
      delay(5000);
    }    
  }
}

/**
 * Boucle
 */
void  loop() {
  
   if (!client.connected()) {
      reconnect();
       
   }
   client.loop();
}

/**
* Déclenche les actions à la réception d'un message
*
*/ 
void callback(char* topic, byte* payload, unsigned int length) {

  int i = 0;
  
  Serial.print("Message recu =>  topic: " + String(topic));
  Serial.println(" | longueur: " + String(length,DEC));
  
  // create character buffer with ending null terminator (string)
  for(i = 0; i < length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';
  
  String msgString = String(message_buff);

  Serial.print("Message: ");
  Serial.println(msgString);
  
}
